<?php

namespace Nitra\AuthBundle\Controller;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\Form;
use Nitra\StoreBundle\Lib\Globals;
use Nitra\BuyerBundle\Document\Buyer;
use Nitra\AuthBundle\Document\AuthSettings;
use Nitra\AuthBundle\Document\notConfirmedBuyer;
use Nitra\AuthBundle\Form\Type\SmsType;
use Nitra\AuthBundle\Form\Type\captchaType;
use Nitra\AuthBundle\Form\Type\newBuyerType;
use Nitra\AuthBundle\Form\Type\codeType;
use Nitra\AuthBundle\Form\Type\loginType;
use Nitra\AuthBundle\Form\Type\ForgotenPasswordType;

class AuthController extends NitraController
{
    protected $buyerRepository              = 'NitraBuyerBundle:Buyer';
    protected $notConfirmedBuyerRepository  = 'NitraAuthBundle:notConfirmedBuyer';

    /**
     * @Route("/nitra_auth", name="nitra_auth")
     */
    public function indexAction(Request $request)
    {
        $session  = $request->getSession();
        $settings = $this->getSettings();
        if ($settings) {
            $allUsersCaptchaReview = $settings->getCaptchaAllUserReview();
            $newUsersCaptchaReview = $settings->getCaptchaNewUserReview();
            $allUsersCaptcha       = $settings->getCaptchaAllUser();
        } else {
            $allUsersCaptchaReview = false;
            $newUsersCaptchaReview = false;
            $allUsersCaptcha       = false;
        }
        $session->set('securityReview', array(
            'allUsersCaptchaReview' => $allUsersCaptchaReview,
            'newUsersCaptchaReview' => $newUsersCaptchaReview,
        ));
        $buyerAuth = $session->get('buyer');
        if ($buyerAuth) {
            return new RedirectResponse($this->generateUrl('privat_office_page'));
        }

        $codeForm = $this->handleCodeForm($request);
        if ($codeForm instanceof Response) {
            return $codeForm;
        }

        $captchaForm = $this->handleCapctchaForm($request);
        if ($captchaForm instanceof Response) {
            return $captchaForm;
        }

        $loginForm = $this->handleLoginForm($request, $captchaForm, $settings);
        if ($loginForm instanceof Response) {
            return $loginForm;
        }

        $phoneForm = $this->handlePhoneForm($request, $codeForm);
        if ($phoneForm instanceof Response) {
            return $phoneForm;
        }

        return $this->render('NitraAuthBundle:Auth:index.html.twig', array(
            'sms'             => ($phoneForm instanceof Form) ? $phoneForm->createView() : null,
            'allUsersCaptcha' => $allUsersCaptcha,
            'login'           => $loginForm->createView(),
        ));
    }

    protected function getLoginForm()
    {
        return new loginType();
    }

    protected function getCodeForm()
    {
        return new codeType();
    }

    protected function getPhoneForm()
    {
        return new SmsType();
    }

    protected function getBuyerForm()
    {
        return new newBuyerType();
    }

    protected function getCaptchaForm()
    {
        return new captchaType();
    }

    protected function getForgotenPasswordForm()
    {
        return new ForgotenPasswordType();
    }

    protected function handleLoginForm(Request $request, Form $captchaForm, AuthSettings $settings = null)
    {
        if ($settings) {
            $allUsersCaptcha = $settings->getCaptchaAllUser();
            $newUsersCaptcha = $settings->getCaptchaNewUser();
        } else {
            $allUsersCaptcha = false;
            $newUsersCaptcha = false;
        }
        $loginForm = $this->createForm($this->getLoginForm(), null, array(
            'captcha' => $allUsersCaptcha,
        ));
        $loginForm->handleRequest($request);
        if ($loginForm->isSubmitted()) {
            if ($loginForm->isValid()) {
                $login    = $loginForm->get('login')->getData();
                $password = $loginForm->get('password')->getData();
                $hashPass = hash('sha256', $password);
                $user     = $this->findUser($login);

                if ($user) {
                    if ($user->getPassword() == $hashPass) {
                        if ($newUsersCaptcha && ($user->getStatus() == 'passive')) {
                            $this->setSession($user, $temp = true);
                            return $this->render('NitraAuthBundle:Auth:captcha.html.twig', array(
                                'captcha' => $captchaForm->createView(),
                            ));
                        }
                        $this->setSession($user);
                        return new JsonResponse(array(
                            'valid' => true,
                        ));
                    } else {
                        $text = $this->get('translator')->trans('error.wrongPassword', array(), 'NitraAuthBundle');
                        $loginForm->get('password')->addError(new FormError($text));
                    }
                } else {
                    $text = $this->get('translator')->trans('error.wrongLogin', array(), 'NitraAuthBundle');
                    $loginForm->get('login')->addError(new FormError($text));
                }
            }
        }

        return $loginForm;
    }

    protected function handlePhoneForm(Request $request, Form $codeForm)
    {
        $smsForm = $this->createForm($this->getPhoneForm(), null, array());
        $smsForm->handleRequest($request);
        if ($smsForm->isSubmitted() && $smsForm->isValid()) {
            $phone = $smsForm->get('phone')->getData();
            $kod   = $this->getPassSmsAction($phone);
            if ($kod) {
                return $this->render('NitraAuthBundle:Auth:smsCode.html.twig', array(
                    'kod'  => $kod,
                    'code' => $codeForm->createView(),
                ));
            } else {
                $text = $this->get('translator')->trans('error.wrongPhone', array(), 'NitraAuthBundle');
                $smsForm->get('phone')->addError(new FormError($text));
            }
        }

        return $smsForm;
    }

    protected function handleCodeForm(Request $request)
    {
        $codeForm = $this->createForm($this->getCodeForm(), null, array());
        $codeForm->handleRequest($request);
        if ($codeForm->isSubmitted()) {
            $kod = $request->getSession()->get('sms_code');
            if ($codeForm->isValid()) {
                $code = $codeForm->get('code')->getData();
                if ($code == $kod) {
                    $buyer = array(
                        'id' => $request->getSession()->get('buyer_id'),
                    );
                    $request->getSession()->set('buyer', $buyer);
                    return new JsonResponse(array(
                        'valid' => true,
                    ));
                } else {
                    $text = $this->get('translator')->trans('error.wrongCode', array(), 'NitraAuthBundle');
                    $codeForm->get('code')->addError(new FormError($text));
                    return $this->render('NitraAuthBundle:Auth:smsCode.html.twig', array(
                        'kod'  => $kod,
                        'code' => $codeForm->createView(),
                    ));
                }
            } else {
                return $this->render('NitraAuthBundle:Auth:smsCode.html.twig', array(
                    'kod'  => $kod,
                    'code' => $codeForm->createView(),
                ));
            }
        }

        return $codeForm;
    }

    protected function handleCapctchaForm(Request $request)
    {
        $captchaForm = $this->createForm($this->getCaptchaForm(), null, array());
        $captchaForm->handleRequest($request);
        if ($captchaForm->isSubmitted()) {
            if ($captchaForm->isValid()) {
                $request->getSession()->set('buyer', $request->getSession()->get('tempBuyer'));
                $request->getSession()->remove('tempBuyer');
                return new JsonResponse(array(
                    'valid' => true,
                ));
            }
            return $this->render('NitraAuthBundle:Auth:captcha.html.twig', array(
                'captcha' => $captchaForm->createView(),
            ));
        }

        return $captchaForm;
    }

    /**
     * @param string $username Phone or E-mail
     * @return Buyer|null
     */
    protected function findUser($username)
    {
        $phone = preg_replace('/[^0-9]/', '', $username);
        $qb    = $this->getDocumentManager()->createQueryBuilder($this->buyerRepository);
        $qb ->addOr($qb->expr()->field('phone')->equals($phone))
            ->addOr($qb->expr()->field('email')->equals($username));

        return $qb->getQuery()->execute()->getSingleResult();
    }

    /**
     * Регистрация
     * @Route("/registration", name="registration")
     */
    public function registrationAction(Request $request)
    {
        $cities = $this->getTetradka()->getCities();

        $form = $this->createForm($this->getBuyerForm(), null, array(
            'cities' => $cities,
        ));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $phone     = preg_replace('/[^0-9]/', '', $form->get('phone')->getData());
            $name      = $form->get('name')->getData();
            $email     = $form->get('email')->getData();
            $city      = $form->has('city') ? $form->get('city')->getData() : null;
            $password  = $form->get('password')->getData();
            $password2 = $form->get('password2')->getData();

            $simpleBuyer = false;

            if ($password != $password2) {
                $text = $this->get('translator')->trans('error.passNotEqual', array(), 'NitraAuthBundle');
                $form->get('password2')->addError(new FormError($text));
            }
            $minPassLength = $this->container->hasParameter('registration_min_pass_length') ? $this->container->getParameter('registration_min_pass_length') : 6;
            if (strlen($password) < $minPassLength) {
                $text = $this->get('translator')->trans('error.passIsShorted', array(
                    '%q%'   => $minPassLength,
                ), 'NitraAuthBundle');
                $form->get('password')->addError(new FormError($text));
            }
            $buyerByEmail = $this->getIsSetBuyerByEmail($email);
            // если покупатель уже есть и он уже подтвержден (если регистрировался)
            // и если покупатель только подписался на новости или на уведомления, и больше никаких действий не совершал
            if (($buyerByEmail instanceof Buyer) && (!$buyerByEmail->getName() && !$buyerByEmail->getPassword() && !$buyerByEmail->getPhone())) {
                $simpleBuyer = $buyerByEmail;
            } elseif ($buyerByEmail != false) {
                $text = $this->get('translator')->trans('error.emailExists', array(), 'NitraAuthBundle');
                $form->get('email')->addError(new FormError($text));
            }
            if ($phone) {
                $notConfirmedBuyer = $this->getDocumentManager()->getRepository($this->notConfirmedBuyerRepository)->findOneByPhone($phone);
                if ($this->getBuyerByPhone($phone) || $notConfirmedBuyer) {
                    $text = $this->get('translator')->trans('error.phoneExists', array(), 'NitraAuthBundle');
                    $form->get('phone')->addError(new FormError($text));
                }
            }
            if (!(count($form->get('email')->getErrors()) || count($form->get('phone')->getErrors()) || count($form->get('password2')->getErrors()) || count($form->get('password')->getErrors()))) {
                if ($simpleBuyer) {
                    $buyer = $simpleBuyer;
                } else {
                    $buyer = new notConfirmedBuyer();
                }

                $buyer->setName($name);
                if ($city) {
                    $buyer->setCityId($city);
                    foreach ($cities as $regionCities) {
                        if (isset($regionCities[$city])) {
                            $buyer->setCityName($regionCities[$city]);
                            break;
                        }
                    }
                }
                $buyer->setEmail($email);
                $buyer->setPhone($phone);
                $buyer->setPassword(hash('sha256', $password));
                if (!$simpleBuyer || !$email) {
                    $this->sendBuyerConfirm($buyer);
                }
                $this->getDocumentManager()->persist($buyer);
                $this->getDocumentManager()->flush();

                if ($simpleBuyer) {
                    return new JsonResponse(array(
                        'msg' => $this->get('translator')->trans('registrationComplete', array(), 'NitraAuthBundle'),
                    ));
                } else {
                    return new JsonResponse(array(
                        'msg' => $this->get('translator')->trans('confirmLink', array(), 'NitraAuthBundle'),
                    ));
                }
            }
        }

        return $this->render('NitraAuthBundle:Auth:registration.html.twig',
                             array(
                'form' => $form->createView(),
        ));
    }

    /**
     * @param Buyer $buyer
     * @return bool
     */
    protected function sendBuyerConfirm(&$buyer)
    {
        $code = hash('sha256', $buyer->getEmail() . $buyer->getPhone() . $buyer->getPassword());
        $buyer->setConfirmPassword($code);

        $body = $this->renderView('NitraAuthBundle:Auth:sendMail.html.twig', array(
            'name' => $buyer->getName(),
            'link' => $this->generateUrl('confirm_user', array(
                'confirmPassword' => $code,
            ), true),
        ));

        $store        = Globals::getStore();
        $mailingEmail = $store['mailingEmail'];
        $subject      = 'Подтверждение пароля';
        $message      = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($mailingEmail)
            ->setTo($buyer->getEmail())
            ->setContentType('text/html')
            ->setBody($body);

        return $this->get('mailer')->send($message);
    }

    protected function getBuyerFromTetradka($id = null, $phone = null, $orders = false)
    {
        if (!$id && !$phone) {
            return false;
        }
        $dm       = $this->container->get('doctrine.odm.mongodb.document_manager');
        $hostName = $this->container->hasParameter('tetradka') ? $this->container->getParameter('tetradka') : 'localhost';

        $response = $dm->getRepository($this->buyerRepository)->getBuyerFromTetradka($hostName, $id, $phone, $orders);
        if ($response && ($response['status'] == 'success')) {
            return $response;
        } elseif ($response && ($response['status'] == 'error')) {
            return $this->get('translator')->trans($response['msg'], array(), "NitraBuyerBundle");
        }
    }

    protected function sendSms($kod, $phone)
    {
        /* заглушка */
        return true;

        $text       = $this->get('translator')->trans('code.forLogin', array(
            '%kod%' => $kod,
        ), "NitraStoreBundle");
        $login      = $this->container->getParameter('sms_login');
        $password   = $this->container->getParameter('sms_password');
        $sender     = $this->container->getParameter('sms_sender');
        $prep_query = array(
            'login'    => $login,
            'psw'      => $password,
            'phones'   => $phone,
            'mes'      => $text,
            'sender'   => $sender,
            'translit' => 0,
            'cost'     => 2,
            'fmt'      => 1,
            'charset'  => 'utf-8'
        );

        $generated_params = http_build_query($prep_query);

        if ($generated_params === false) {
            return false;
        }

        $host_name = 'http://smsc.ru/sys/send.php';

        $result = file_get_contents($host_name . '?' . $generated_params);

        if ($result === false) {
            return false;
        }

        // запрос прошел.
        $r = explode(',', $result);

        // если сервер вернул ошибку
        if (isset($r[1]) && $r[1] < 0) {
            return false;
        }

        return true;
    }

    /**
     * @param string $phone
     * @return Buyer|null
     */
    protected function getBuyerByPhone($phone)
    {
        return $this->getDocumentManager()->getRepository($this->buyerRepository)->findOneByPhone(preg_replace('/[^0-9]/', '', $phone));
    }

    protected function getPassSmsAction($phone)
    {
        $session = $this->getRequest()->getSession();

        $buyer = $this->getBuyerByPhone($phone);

        if (!$buyer) {
            return null;
        }

        $session->set('buyer_id', $buyer->getId());

        $kod = rand(100000, 999999);

        if (!$this->sendSms($kod, $phone)) {
            return;
        }

        $session->set('sms_code', $kod);

        return $kod;
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request)
    {
        $session = $this->getRequest()->getSession();
        $session->remove('securityReview');
        $session->remove('genemu_form.captcha.options');
        $session->remove('buyer');

        return new Response();
    }

    /**
     * функция получения всех городов из тетрадки
     *
     * @return array cities
     */
    public function getCitiesFromTetradka()
    {
        $this->getTetradka()->getCities(true);
    }

    /**
     * @param string $cName
     * @param string $rName
     */
    protected function formatCityName($cName, $rName)
    {
        return "$cName ($rName)";
    }

    protected function getIsSetBuyerByEmail($email)
    {
        $notConfirmedBuyer = $this->getDocumentManager()
            ->getRepository($this->notConfirmedBuyerRepository)
            ->findOneByEmail(new \MongoRegex('/^' . $email . '$/i'));
        if ($notConfirmedBuyer) {
            return $notConfirmedBuyer;
        }
        $buyer = $this->getDocumentManager()
            ->getRepository($this->buyerRepository)
            ->findOneByEmail(new \MongoRegex('/^' . $email . '$/i'));
        if ($buyer) {
            return $buyer;
        }
        return false;
    }

    /**
     * @Route("/confirm-user/{confirmPassword}", name="confirm_user")
     */
    public function confirmBuyer($confirmPassword, Request $request)
    {
        $confirmedBuyer = $confirmPassword
            ? $this->getDocumentManager()->getRepository($this->notConfirmedBuyerRepository)->findOneByConfirmPassword($confirmPassword)
            : null;
        if ($confirmedBuyer) {
            $email    = $confirmedBuyer->getEmail();
            $phone    = $confirmedBuyer->getPhone();
            $name     = $confirmedBuyer->getName();
            $cityId   = $confirmedBuyer->getCityId();
            $cityName = $confirmedBuyer->getCityName();
            $password = $confirmedBuyer->getPassword();
            $this->getDocumentManager()->remove($confirmedBuyer);
            $buyer    = new Buyer();
            $buyer->setEmail($email);
            $buyer->setPassword($password);
            $buyer->setName($name);
            if ($phone) {
                $buyer->setPhone($phone);
            }
            if ($cityId) {
                $buyer->setCityId($cityId);
            }
            if ($cityName) {
                $buyer->setCityName($cityName);
            }
            $buyer->setStatus('passive');
            $this->getDocumentManager()->persist($buyer);
            $this->getDocumentManager()->flush();

            // ищем по новой, так как id будет другой
            $buyer = $this->getDocumentManager()->getRepository($this->buyerRepository)
                ->findOneBy(array(
                    'email'    => $email,
                    'name'     => $name,
                    'password' => $password,
                ));
            $this->setSession($buyer);

            return $this->render('NitraAuthBundle:Auth:confirmSuccesful.html.twig');
        }
        return $this->redirect($this->generateUrl('nitra_store_home_index'));
    }

    /**
     * @Route("/test", name="test")
     */
    public function test()
    {
        $provider = 'https://www.google.com/accounts/o8/ud';
        $method   = 'authenticateBy' . $this->normalizeProvider($this->parseProvider($provider));
        var_dump($method);
        die;
    }

    /**
     * @Route("/social_auth", name="social_auth")
     */
    public function socialAuth(Request $request)
    {
        $token   = $request->request->get('token');
        if (isset($token)) {
            $response = file_get_contents("http://loginza.ru/api/authinfo?token={$token}");
            $data     = json_decode($response, true);
        }
        if (isset($data["error_type"]) || !isset($data["identity"])) {
            return new Response('<script type="text/javascript">window.close();</script>');
        }
        $provider = $data['provider'];
        $identity = $data['identity'];

        $method   = 'authenticateBy' . $this->normalizeProvider($this->parseProvider($provider));
        if (!method_exists($this, $method)) {
            return new Response('<script type="text/javascript">window.close();</script>');
        }

        $buyer = $this->getDocumentManager()->getRepository($this->buyerRepository)->findOneByIdentity($identity);
        if (!$buyer && isset($data['email'])) {
            $buyer = $this->getDocumentManager()->getRepository($this->buyerRepository)->findOneByEmail($data['email']);
        }
        if (!$buyer) {
            $buyer = new Buyer();
            $buyer->setStatus('passive');
            $buyer->setIdentity($data['identity']);
            $this->getDocumentManager()->persist($buyer);
        }
        $this->$method($buyer, $data);

        $this->getDocumentManager()->flush();

        $this->setSession($buyer);

        return new Response('<script type="text/javascript">
            window.opener.updateBuyerButtons();
            window.close();
        </script>');
    }

    /**
     * Parse user provider
     *
     * @param string $provider
     *
     * @return string
     *
     * @throws \Exception
     */
    protected function parseProvider($provider)
    {
        $parsed = parse_url($provider);

        if ($parsed === false) {
            throw new \Exception('Unable to parse provider');
        }

        return $parsed['host'];
    }

    /**
     * Normalize provider for call
     *
     * @param string $provider
     *
     * @return string
     */
    protected function normalizeProvider($provider)
    {
        $words = preg_split('/[^a-z]+/', $provider);

        foreach ($words as &$word) {
            $word = ucfirst($word);
        }

        return implode('', $words);
    }

    /**
     * @param Buyer $buyer
     * @param array $data
     */
    protected function authenticateByVkCom($buyer, $data)
    {
        $name = trim($data['name']['first_name'] . ' ' . $data['name']['last_name']);
        $buyer->setName($name);
    }

    /**
     * @param Buyer $buyer
     * @param array $data
     */
    protected function authenticateByWwwFacebookCom($buyer, $data)
    {
        if (isset($data['email']) && $data['email']) {
            $buyer->setEmail($data['email']);
        }
        $buyer->setName(trim($data['name']['full_name']));
    }

    /**
     * @param Buyer $buyer
     * @param array $data
     */
    protected function authenticateByOdnoklassnikiRu($buyer, $data)
    {
        $buyer->setName(trim($data['name']['full_name']));
    }

    /**
     * @param Buyer $buyer
     * @param array $data
     */
    protected function authenticateByTwitterCom($buyer, $data)
    {
        $buyer->setName(trim($data['name']['full_name']));
    }

    /**
     * @param Buyer $buyer
     * @param array $data
     */
    protected function authenticateByWwwGoogleCom($buyer, $data)
    {
        $name  = trim($data['name']['last_name'] . ' ' . $data['name']['first_name']);
        $buyer->setName($name);
        if (isset($data['email'])) {
            $buyer->setEmail($data['email']);
        }
    }

    protected function setSession($buyer, $temp = false)
    {
        $session = $this->getRequest()->getSession();
        $buyer   = array(
            'id'       => $buyer->getId(),
            'name'     => $buyer->getName(),
            'identity' => $buyer->getIdentity(),
            'newBuyer' => $buyer->getStatus(),
        );
        $temp ? $session->set('tempBuyer', $buyer) : $session->set('buyer', $buyer);
    }

    protected function getCache()
    {
        return $this->container->get('cache_apc');
    }

    protected function getSettings()
    {
        $settingsName = $this->getMongoDatabaseName() . '_auth_settings_' . $this->getStoreHost();
        $settings     = $this->getCache()->fetch($settingsName);
        if (!$settings) {
            $settings = $this->getDocumentManager()->getRepository('NitraAuthBundle:AuthSettings')->findOneBy(array());
            $this->getCache()->save($settingsName, $settings);
        }
        return $settings;
    }

    protected function getStoreHost()
    {
        return $this->container->hasParameter('store_host') ? $this->container->getParameter('store_host') : '';
    }

    protected function getMongoDatabaseName()
    {
        return $this->container->hasParameter('mongo_database_name') ? $this->container->getParameter('mongo_database_name') : '';
    }

    /**
     * Забыли пароль?
     * @Route("forgoten-password", name="forgoten_password")
     * @Template("NitraAuthBundle:Auth:forgotenPassword.html.twig")
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function forgotenPasswordAction(Request $request)
    {
        $dm = $this->getDocumentManager();

        $form = $this->createForm($this->getForgotenPasswordForm());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $minPassLength = $this->container->hasParameter('registration_min_pass_length') ? $this->container->getParameter('registration_min_pass_length') : 6;
            $buyer         = $dm->getRepository($this->buyerRepository)->findOneByEmail($form->get('email')->getData());
            if ($buyer) {
                $password = $this->generateNewPassword($minPassLength);
                if ($this->sendNewPassword($buyer, $password)) {
                    $buyer->setPassword(hash('sha256', $password));
                    $dm->flush($buyer);
                    return new Response($this->get('translator')->trans('forgoten_password.success', array(), 'NitraAuthBundle'));
                } else {
                    return new Response($this->get('translator')->trans('forgoten_password.send_error', array(), 'NitraAuthBundle'));
                }
            } else {
                $form->get('email')->addError(new FormError($this->get('translator')->trans('forgoten_password.user_no_found', array(), 'NitraAuthBundle')));
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @param int $length
     * @return string
     */
    protected function generateNewPassword($length = 6)
    {
        $chars    = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $password = '';
        for ($i = 0; $i < $length; $i ++) {
            $password .= $chars[rand(0, strlen($chars) - 1)];
        }
        return $password;
    }

    /**
     * @param \Nitra\BuyerBundle\Document\Buyer $buyer
     * @param string $password Новы пароль
     * @return bool
     */
    protected function sendNewPassword(Buyer $buyer, $password)
    {
        $store = Globals::getStore();

        $from    = $store['mailingEmail'];
        $to      = $buyer->getEmail();
        $subject = $this->get('translator')->trans('forgoten_password.message_subject', array(
            '%storeName%' => $store['name'],
        ), 'NitraAuthBundle');
        $body    = $this->get('templating')->render('NitraAuthBundle:Auth:emailForgotenPassword.html.twig', array(
            'buyer'    => $buyer,
            'password' => $password,
        ));

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($from)
            ->setTo($to)
            ->setContentType('text/html')
            ->setBody($body);

        return $this->get('mailer')->send($message);
    }
}