AuthBundle
==============


Импорт маршрутизации
--------------------

Импорт маршрутизации:

```yaml
# app/config/routing.yml

nitra_auth:
    resource: "@NitraAuthBundle/Resources/config/routing.yml"
    prefix:   /
```

Включение бандла
-----------------

Добавьте бандл в kernel:

```php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Nitra\AuthBundle\NitraAuthBundle(),
    );
}
```

Размещение на сайте
--------------------

AuthBundle работает совместно с BuyerBundle и использует его документ.
Чтоб включить кнопки входа и регистрации достаточно добавить:

```
{% render controller("NitraBuyerBundle:PrivateOffice/PrivateOffice:enter") %}
```
в base.html.twig.
Например:

```
{% block header_info %}
    {% render controller("NitraInformationBundle:Information/Information:informationMenu") %}   
    {% render controller("NitraBuyerBundle:PrivateOffice/PrivateOffice:enter") %}
{% endblock header_info %}

```