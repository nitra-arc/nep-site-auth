<?php

namespace Nitra\AuthBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ForgotenPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', 'email', array(
            'label'                 => 'forgoten_password.email.label',
            'help'                  => 'forgoten_password.email.help',
            'required'              => true,
            'constraints'           => array(
                new Constraints\Email,
            ),
        ));
        $builder->add('submit', 'submit', array(
            'label'                 => 'forgoten_password.submit',
        ));
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain'    => 'NitraAuthBundle',
        ));
    }

    public function getName()
    {
        return 'forgoten_password';
    }
}