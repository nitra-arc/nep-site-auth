<?php

namespace Nitra\AuthBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoginType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('login', 'text', array(
            'label'       => 'login',
            'required'    => true,
            'constraints' => array(
                new Constraints\Email(),
            )
        ));
        $builder->add('password', 'password', array(
            'label'    => 'password',
            'required' => true,
        ));
        $builder->add('submit', 'submit', array(
            'label' => 'enter',
            'attr'  => array(
                'class' => 'saveform'
            ),
        ));
        if ($options['captcha']) {
            $builder->add('captcha', 'genemu_captcha', array(
                'label' => 'captcha',
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'login_auth';
    }

    /**
     * @inheritdoc
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'NitraAuthBundle',
            'captcha'            => false,
        ));
    }
}