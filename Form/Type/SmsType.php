<?php

namespace Nitra\AuthBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;

class SmsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('phone', 'masked_input', array(
            'label'                 => 'phone',
            'mask'                  => '+38(999)999-99-99',
            'translation_domain'    => $this->getTranslationDomain(),
            'required'              => true,
            'constraints'           => array(
                new Constraints\NotBlank()
            ),
        ));
        $builder->add('submit', 'submit', array(
            'label'                 => 'getCode',
            'translation_domain'    => $this->getTranslationDomain(),
            'attr'                  => array(
                'class' => 'saveform'
            ),
        ));
    }

    protected function getTranslationDomain()
    {
        return 'NitraAuthBundle';
    }

    public function getName()
    {
        return 'sms_auth';
    }
}