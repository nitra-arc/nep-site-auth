<?php

namespace Nitra\AuthBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;
use Nitra\StoreBundle\Lib\Globals;

class newBuyerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $translator = Globals::$container->get('translator');
        $select2NoMatches = $translator->trans('select2.formatNoMatches', array(), 'NitraStoreBundle');
        $select2ShortInput = $translator->trans('select2.formatInputTooShort', array(), 'NitraStoreBundle');
        $select2BigSelection = $translator->trans('select2.formatSelectionTooBig', array(), 'NitraStoreBundle');
        $builder
            ->add('name', 'text', array(
                'label'                 => 'name',
            ))
            ->add('email', 'email', array(
                'label'                 => 'email',
            ))
            ->add('phone', 'masked_input', array(
                'label'                 => 'phone',
                'mask'                  => '+38(999)999-99-99',
                'constraints'           => array(
                    new Constraints\NotBlank()
                ),
                'required'              => true,
            ))
            ->add('password', 'password', array(
                'label'                 => 'password',
            ))
            ->add('password2', 'password', array(
                'label'                 => 'password2',
                'mapped'                => false,
            ))
            ->add('city', 'genemu_jqueryselect2_choice', array(
                'choices'               => $options['cities'],
                'label'                 => 'city',
                'mapped'                => false,
                'required'              => false,
                'configs'               => array(
                    'placeholder'           => '',
                    'width'                 => '310px',
                    'minimumInputLength'    => 1,
                    'formatNoMatches'       => 'function (input, min) { var n = min - input.length; return "' . $select2NoMatches . '"; }',
                    'formatSelectionTooBig' => 'function (n) { return "' . $select2BigSelection . '"; }',
                    'formatInputTooShort'   => 'function (input, min) { var n = min - input.length; return "' . $select2ShortInput . '"; }',
                ),
            ))
            ->add('submit', 'submit', array(
                'label'                 => 'submit',
                'attr'                  => array(
                    'class'                 => 'saveform'
                ),
        ));
    }

    public function getName()
    {
        return 'new_buyer';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'cities'                => array(),
            'translation_domain'    => 'NitraAuthBundle',
        ));
    }
}