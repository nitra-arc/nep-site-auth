<?php
namespace Nitra\AuthBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;

class captchaType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('captcha', 'genemu_captcha', array('label' => 'captcha',
                                             'translation_domain' => 'NitraAuthBundle'))
            ->add('submit', 'submit', array(
                                     'translation_domain' => $this->getTranslationDomain(),
                                     'label' => 'submit',
        ));
    }

    public function getTranslationDomain()
    {
        return 'NitraAuthBundle';
    }

    public function getName()
    {
        return 'new_user_captcha';
    }

}

?>
