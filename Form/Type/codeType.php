<?php
namespace Nitra\AuthBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;

class codeType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('code', 'integer', array(
                                     'required' => true,
                                     'label' => 'code.enter',
                                     'translation_domain' => $this->getTranslationDomain(),
                                     'attr' => array(
                                                              'placeholder' => '123456',
                                     ),
            ))
            ->add('submit', 'submit', array(
                                     'translation_domain' => $this->getTranslationDomain(),
                                     'label' => 'submit',
        ));
    }

    public function getTranslationDomain()
    {
        return 'NitraAuthBundle';
    }

    public function getName()
    {
        return 'code_sms';
    }

}

?>
