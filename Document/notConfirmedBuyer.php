<?php
namespace Nitra\AuthBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections as Collections;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Bundle\MongoDBBundle\Validator\Constraints\Unique as MongoDBUnique;

/**
 * @MongoDB\Document
 * 
 *
 */
class notConfirmedBuyer
{

    /**
     * @MongoDB\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @MongoDB\String 
     * @Gedmo\Translatable
     * @Assert\NotBlank
     */
    private $name;


    /**
     * @MongoDB\String
     * @Assert\Email
     */
    private $email;

    /**
     * @MongoDB\String
     */
    private $phone;

     /**
     * @MongoDB\String
     */
    private $cityName;
    
     /**
     * @MongoDB\Int
     */
    private $cityId;
    
    /**
     * @MongoDB\String
     * 
     */
    private $password;
    
      /**
     * @MongoDB\String
     * 
     */
    private $confirmPassword;

   

      
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = preg_replace('/[^0-9]/', '', $phone);
        return $this;
    }

    /**
     * Get phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get password
     *
     * @return string $password
     */
    public function getPassword()
    {
        return $this->password;
    }


    /**
     * Set cityName
     *
     * @param string $cityName
     * @return self
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;
        return $this;
    }

    /**
     * Get cityName
     *
     * @return string $cityName
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * Set cityId
     *
     * @param int $cityId
     * @return self
     */
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;
        return $this;
    }

    /**
     * Get cityId
     *
     * @return int $cityId
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * Set confirmPassword
     *
     * @param string $confirmPassword
     * @return self
     */
    public function setConfirmPassword($confirmPassword)
    {
        $this->confirmPassword = $confirmPassword;
        return $this;
    }

    /**
     * Get confirmPassword
     *
     * @return string $confirmPassword
     */
    public function getConfirmPassword()
    {
        return $this->confirmPassword;
    }
}
