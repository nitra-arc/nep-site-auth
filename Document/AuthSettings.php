<?php
namespace Nitra\AuthBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections as Collections;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document
 */
class AuthSettings
{

    /**
     * @MongoDB\Id(strategy="AUTO")
     * 
     */
    private $id;

    /**
     * 
     * @MongoDB\Boolean 
     * @Assert\NotBlank
     * 
     */
    private $captchaAllUser;

    /**
     * 
     * @MongoDB\Boolean 
     * @Assert\NotBlank
     * 
     */
    private $captchaNewUser;

    /**
     * 
     * @MongoDB\Boolean 
     * @Assert\NotBlank
     * 
     */
    private $captchaAllUserReview;

    /**
     * 
     * @MongoDB\Boolean 
     * @Assert\NotBlank
     * 
     */
    private $captchaNewUserReview;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set captchaAllUser
     *
     * @param boolean $captchaAllUser
     * @return self
     */
    public function setCaptchaAllUser($captchaAllUser)
    {
        $this->captchaAllUser = $captchaAllUser;
        return $this;
    }

    /**
     * Get captchaAllUser
     *
     * @return boolean $captchaAllUser
     */
    public function getCaptchaAllUser()
    {
        return $this->captchaAllUser;
    }

    /**
     * Set captchaNewUser
     *
     * @param boolean $captchaNewUser
     * @return self
     */
    public function setCaptchaNewUser($captchaNewUser)
    {
        $this->captchaNewUser = $captchaNewUser;
        return $this;
    }

    /**
     * Get captchaNewUser
     *
     * @return boolean $captchaNewUser
     */
    public function getCaptchaNewUser()
    {
        return $this->captchaNewUser;
    }

    /**
     * Set captchaAllUserReview
     *
     * @param boolean $captchaAllUserReview
     * @return self
     */
    public function setCaptchaAllUserReview($captchaAllUserReview)
    {
        $this->captchaAllUserReview = $captchaAllUserReview;
        return $this;
    }

    /**
     * Get captchaAllUserReview
     *
     * @return boolean $captchaAllUserReview
     */
    public function getCaptchaAllUserReview()
    {
        return $this->captchaAllUserReview;
    }

    /**
     * Set captchaNewUserReview
     *
     * @param boolean $captchaNewUserReview
     * @return self
     */
    public function setCaptchaNewUserReview($captchaNewUserReview)
    {
        $this->captchaNewUserReview = $captchaNewUserReview;
        return $this;
    }

    /**
     * Get captchaNewUserReview
     *
     * @return boolean $captchaNewUserReview
     */
    public function getCaptchaNewUserReview()
    {
        return $this->captchaNewUserReview;
    }
}
